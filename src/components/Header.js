import React from 'react'

/*
        <div className="logo">
            <span className="icon fa-american-sign-language-interpreting"></span>
        </div>
*/
const Header = (props) => (
    <header id="header" style={props.timeout ? {display: 'none'} : {}}>
        <div className="logo">
            <h1>Majiska Web Agency</h1>
        </div>
        <div className="content">
            <div className="inner">
                <h1>Engage your digital audience</h1>
                <p>
                    Grow your business by creating impact on your web applications.
                </p>
                <p>In <strong><a href="javascript:;" onClick={() => {props.onOpenArticle('about')}}>Majiska</a></strong>,
                we are passionate to deliver beautiful, secured and performant solutions.  <br />
                <a href="javascript:;" onClick={() => {props.onOpenArticle('contact')}}>Let us know your problem.</a>
                </p>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('intro')}}>Intro</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('work')}}>Work</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('about')}}>About</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('contact')}}>Contact</a></li>
            </ul>
        </nav>
    </header>
)

Header.propTypes = {
    onOpenArticle: React.PropTypes.func,
    timeout: React.PropTypes.bool
}

export default Header
