import React from 'react'

/*<p className="copyright">&copy; Gatsby Starter - Dimension. Design: <a href="https://html5up.net">HTML5 UP</a>. 
        Built with: <a href="https://www.gatsbyjs.org/">Gatsby.js</a></p>*/
const Footer = (props) => (
    <footer id="footer" style={props.timeout ? {display: 'none'} : {}}>
        <p className="copyright">
            &copy; 2018 Majiska Solutions <span className="fa fa-envelope"></span>: hello@themajiska.com <span className="fa fa-phone"></span>: +60193029570
        </p>
        
    </footer>
)

Footer.propTypes = {
    timeout: React.PropTypes.bool
}

export default Footer
